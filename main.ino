// Ethernet shield libraries
#include <Ethernet.h>
#include <EthernetUdp.h>
// 5050 cms leds lib
#include <Adafruit_NeoPixel.h>
// Time manipulation libs
#include <Time.h>
#include <TimeLib.h>
//Pseudo threading lib
#include <StaticThreadController.h>
#include <Thread.h>
#include <ThreadController.h>

// Random MAC Adress as shield has no one.
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
unsigned int localPort = 8888;       // local port to listen for UDP packets

char timeServer[] = "time.nist.gov"; // NTP server

const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets

// A UDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

//Using 8 pins, 6 numbers and 2 colons blinking on seconds and minutes
#define second2  2 //Sixth digit, seconds unit
#define second1  3 //Fifth digit, seconds tens
#define secondColon 4 // Seconds colon
#define minute2  5 // Fourth digit, minutes unit.
#define minute1  6 // Third digit, minutes tens.
#define minuteComma  7 //Minutes colon
#define hour2 8 // Second digit, hours unit
#define hour1 9 // First digit, hours tens.

// Each led strip contains 10 leds.
#define NUMPIXELS      10
//Pixels initialization
Adafruit_NeoPixel pixelSecond2 = Adafruit_NeoPixel(NUMPIXELS, second2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelSecond1 = Adafruit_NeoPixel(NUMPIXELS, second1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelsecondColon = Adafruit_NeoPixel(NUMPIXELS, secondColon, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelMinute2 = Adafruit_NeoPixel(NUMPIXELS, minute2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelMinute1 = Adafruit_NeoPixel(NUMPIXELS, minute1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelMinuteComma = Adafruit_NeoPixel(NUMPIXELS, minuteComma, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelHour2 = Adafruit_NeoPixel(NUMPIXELS, hour2, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixelHour1 = Adafruit_NeoPixel(NUMPIXELS, hour1, NEO_GRB + NEO_KHZ800);
Adafruit_NeoPixel pixels[] = {pixelSecond2, pixelSecond1, pixelMinute2, pixelMinute1, pixelHour2, pixelHour1};
//Time state
int seconds;
int minutes;
int hours;
//Allows to request ntp server every day to resynchronize.
int dayCount = 0;

//Threads
Thread blinkingLedThread = Thread();
Thread startEthernetThread = Thread();
Thread startUdpThread = Thread();
Thread initializeTimeThread = Thread();
Thread updateDisplayThread = Thread();

ThreadController controller = ThreadController();

void setup() {
  blinkingLedThread.enabled=true;
  blinkingLedThread.setInterval(100);
  blinkingLedThread.onRun(randomBlink);

  startEthernetThread.enabled=true;
  startEthernetThread.setInterval(2000);
  startEthernetThread.onRun(startEthernet);

  startUdpThread.enabled=false;
  startUdpThread.setInterval(500);
  startUdpThread.onRun(startUdp);

  initializeTimeThread.enabled=false;
  initializeTimeThread.setInterval(500);
  initializeTimeThread.onRun(initializeTime);

  updateDisplayThread.enabled=false;
  updateDisplayThread.setInterval(1000);
  updateDisplayThread.onRun(updateDisplay);

  controller.add(&blinkingLedThread);
  controller.add(&startEthernetThread);
  controller.add(&startUdpThread);
  controller.add(&initializeTimeThread);
  controller.add(&updateDisplayThread);
  
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect.
  }
  Serial.println("Serial intialized");
  pixelSecond2.begin();
  pixelSecond1.begin();
  pixelsecondColon.begin();
  pixelMinute2.begin();
  pixelMinute1.begin();
  pixelMinuteComma.begin();
  pixelHour2.begin();
  pixelHour1.begin();
}

void loop() {
  controller.run();
  if(dayCount == 3600*24){
    initializeTimeThread.enabled=true;
  }
}

void startEthernet(){
  Serial.println("Starting Ethernet");
  // start Ethernet and UDP
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // will reloop in a few seconds
  } else {
    Serial.println("Ethernet started");
    startEthernetThread.enabled=false;
    startUdpThread.enabled=true;
  }
}

void startUdp(){
    Udp.begin(localPort);
    startUdpThread.enabled=false;
    initializeTimeThread.enabled=true;
}

void updateDisplay(){
  Serial.println("Update display");
  dayCount++;
  seconds = second();
  minutes = minute();
  hours = hour();
  displaySecond2();
  displaySecond1();
  displayMinute2();
  displayMinute1();
  displayHour2();
  displayHour1();
  Serial.print(hours);
  Serial.print(":");
  Serial.print(minutes);
  Serial.print(":");
  Serial.println(seconds);
}

void displaySecond2() {

  int pinNr = seconds % 10;
  pixelSecond2.setPixelColor(pinNr, pixelSecond2.Color(255, 215, 0)); // Moderately bright green color.
  pixelSecond2.show(); // This sends the updated pixel color to the hardware.
  pixelSecond2.setPixelColor(pinNr, pixelSecond2.Color(0, 0, 0));
}
void displaySecond1() {
  int pinNr = seconds / 10;
  pixelSecond1.setPixelColor(pinNr, pixelSecond1.Color(255, 215, 0 )); // Moderately bright green color.
  pixelSecond1.show(); // This sends the updated pixel color to the hardware.
  pixelSecond1.setPixelColor(pinNr, pixelSecond1.Color(0, 0, 0));
}

void displayMinute2() {

  int pinNr = minutes % 10;
  pixelMinute2.setPixelColor(pinNr, pixelMinute2.Color(255, 140, 0)); // Moderately bright green color.
  pixelMinute2.show(); // This sends the updated pixel color to the hardware.
  pixelMinute2.setPixelColor(pinNr, pixelMinute2.Color(0, 0, 0));
}
void displayMinute1() {
  int pinNr = minutes / 10;
  pixelMinute1.setPixelColor(pinNr, pixelMinute1.Color(255, 140, 0)); // Moderately bright green color.
  pixelMinute1.show(); // This sends the updated pixel color to the hardware.
  pixelMinute1.setPixelColor(pinNr, pixelMinute1.Color(0, 0, 0));
}
void displayHour2() {
  int pinNr = hours % 10;
  pixelHour2.setPixelColor(pinNr, pixelHour2.Color(255, 69, 0));
  pixelHour2.show(); //
  pixelHour2.setPixelColor(pinNr, pixelHour2.Color(0, 0, 0));

}
void displayHour1() {
  int pinNr = hours / 10;
  pixelHour1.setPixelColor(pinNr, pixelHour1.Color(255, 69, 0));
  pixelHour1.show();
  pixelHour1.setPixelColor(pinNr, pixelHour1.Color(0, 0, 0));
}

bool initializeTime() {
  Serial.println("initialize time");
  bool timeinitialized = false;
  //While time is not initialized, randomly blinking leds to show status.
  //Initialized times out after 5 minutes.
    sendNTPpacket(timeServer);
    delay(150);
    timeinitialized = readTime();
    if(timeinitialized){
      Serial.println("Time initialized");
     blinkingLedThread.enabled=false;
     initializeTimeThread.enabled=false;
     updateDisplayThread.enabled=true;
    }
}

void randomBlink() {
  Serial.println("blink");
  for (int i = 0; i < 7; i++) {
    long pinNr = random(0, 11);
    pixels[i].setPixelColor(pinNr, pixels[i].Color(random(0, 256), random(0, 256), random(0, 256)));
    pixels[i].show();
    pixels[i].setPixelColor(pinNr, pixels[i].Color(0, 0, 0));

  }
}


// send an NTP request to the time server at the given address
void sendNTPpacket(char* address) {
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
  Serial.println("packet sent");
}
boolean readTime() {
  if (Udp.parsePacket()) {
    Serial.println("packet received");
    // We've received a packet, read the data from it
    Udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    // the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, extract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
    Serial.print("Seconds since Jan 1 1900 = ");
    Serial.println(secsSince1900);

    // now convert NTP time into everyday time:
    Serial.print("Unix time = ");
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    epoch = epoch + (3600 * 2); //add 2 hours to match GMT +2
    // print Unix time:
    setTime(epoch);
    Serial.println(epoch);


    // print the hour, minute and second:
    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print((epoch  % 86400L) / 3600); // print the hour (86400 equals secs per day)
    Serial.print(':');
    if (((epoch % 3600) / 60) < 10) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print((epoch  % 3600) / 60); // print the minute (3600 equals secs per minute)
    Serial.print(':');
    if ((epoch % 60) < 10) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(epoch % 60); // print the second
    return true;
  } else {
    return false;
  }
}


